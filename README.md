import java.util.Scanner;

public class Ordenar_Numeros_Por_Binario {

	private static Scanner teclado;

	

	public static int cantidadUnosEnBinario(int numero) {
		int divisor = 2;
		int cantidadDeUnos = 0;
		while (numero >= divisor) {
			if (numero % 2 == 1)
				cantidadDeUnos++;
			numero = numero / 2;

		}
		cantidadDeUnos++;
		return cantidadDeUnos;
	}

	public static int[] ordenarNumeros(int[] n) {
		
		int i = 0;
		int aux[] = new int[n.length];
		int auxNumero = 0;
		int auxNum = 0;
		//calculamos los 1 del codificarlo en binario y lo metemos en un vector
		// aux[]
		for (i = 0; i <= n.length - 1; i++) 
			aux[i] = cantidadUnosEnBinario(n[i]);
		
		// ordenamos aux[] y n[] de mayor a menor según números de 1 y
		// de nenor a mayor en decimal en el vector n[]

		for (i = 0; i < n.length - 1; i++) {
			for (int x = i + 1; x < n.length; x++) {
				if (aux[x] > aux[i]) {
					
					auxNumero = aux[i];
					aux[i] = aux[x];
					aux[x] = auxNumero;
				 auxNum = n[i];
				 n[i] = n[x];
				 n[x] = auxNum;
				} else if (aux[x] == aux[i]) {
				    if (n[x] < n[i]) {
				      auxNum = n[i];
				      n[i] = n[x];
				      n[x] = auxNum;
				    }
				}

			}
		}

		return n;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int cantidadNumeros = 0;

		try {
			teclado = new Scanner(System.in);
			System.out.printf("Introduzca número entero que determine " + " el número máximo de enteros a ordenar");
			cantidadNumeros = teclado.nextInt();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int[] numeros = new int[cantidadNumeros];
		int[] numerosOrdenados = new int[cantidadNumeros];
		int i;
		for (i = 0; i < numeros.length; i++) {

			try {
				System.out.printf("Introduzca número entero ");
				numeros[i] = teclado.nextInt();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (i = 0; i < numeros.length; i++) {
			System.out.println((int) numeros[i]);
		}

		numerosOrdenados = ordenarNumeros(numeros);
		System.out.println("YA ORDENADA : ");

		for (i = 0; i < numerosOrdenados.length; i++) {
			System.out.println((int) numerosOrdenados[i]);
		}
	}

}
